package be.kdg.examen.gedistribueerde.client;

import be.kdg.examen.gedistribueerde.communication.MessageManager;
import be.kdg.examen.gedistribueerde.communication.MethodCallMessage;
import be.kdg.examen.gedistribueerde.communication.NetworkAddress;
import be.kdg.examen.gedistribueerde.server.Server;

public class ServerStub implements Server {
    private final NetworkAddress serverAddress;
    private final MessageManager messageManager;

    public ServerStub(NetworkAddress contactsAddress) {
        this.serverAddress = contactsAddress;
        this.messageManager = new MessageManager();
    }

    private void checkEmptyReply() {
        String value = "";
        while (!"Ok".equals(value)) {
            MethodCallMessage reply = messageManager.wReceive();
            if (!"result".equals(reply.getMethodName())) {
                continue;
            }
            value = reply.getParameter("result");
        }
    }
    public void log(Document document) {
        MethodCallMessage message = new MethodCallMessage(messageManager.getMyAddress(),"log");
        message.setParameter("document",document.getText());
        messageManager.send(message, serverAddress);
        checkEmptyReply();
    }

    @Override
    public Document create(String s) {
        MethodCallMessage message = new MethodCallMessage(messageManager.getMyAddress(),"create");
        message.setParameter("s", s);
        messageManager.send(message,serverAddress);
        MethodCallMessage reply = messageManager.wReceive();
        if (!"result".equals(reply.getMethodName())) {
            return null;
        }
        return new DocumentImpl(reply.getParameter("result"));
    }

    @Override
    public void toUpper(Document document) {
        MethodCallMessage message = new MethodCallMessage(messageManager.getMyAddress(),"toUpper");
        message.setParameter("document",document.getText());
        messageManager.send(message, serverAddress);
        MethodCallMessage reply = messageManager.wReceive();
        if ("result".equals(reply.getMethodName())) {
            document.setText(reply.getParameter("result"));
        }
    }

    @Override
    public void toLower(Document document) {
        MethodCallMessage message = new MethodCallMessage(messageManager.getMyAddress(),"toLower");
        message.setParameter("document",document.getText());
        messageManager.send(message, serverAddress);
        MethodCallMessage reply = messageManager.wReceive();
        if ("result".equals(reply.getMethodName())) {
            document.setText(reply.getParameter("result"));
        }
    }

    @Override
    public void type(Document document, String text) {
        MethodCallMessage message = new MethodCallMessage(messageManager.getMyAddress(),"type");
        message.setParameter("document",document.getText());
        message.setParameter("text",text);
        messageManager.send(message, serverAddress);
        while (true) {
            MethodCallMessage reply = messageManager.wReceive();
            if ("result".equals(reply.getMethodName())) {
                document.setText(reply.getParameter("result"));
            }
        }
    }
}
