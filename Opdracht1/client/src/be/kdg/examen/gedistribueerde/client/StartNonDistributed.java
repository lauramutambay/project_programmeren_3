package be.kdg.examen.gedistribueerde.client;

import be.kdg.examen.gedistribueerde.communication.NetworkAddress;

public class StartNonDistributed {
    public static void main(String[] args) {
        if (args.length != 2) {
            System.err.println("Usage: java Client <contactsIP> <contactsPort>");
            System.exit(1);
        }
        int port = Integer.parseInt(args[1]);
        NetworkAddress serverAddress = new NetworkAddress(args[0], port);
        DocumentImpl document = new DocumentImpl();
        Client client = new Client(
                new ServerStub(serverAddress),
                document);
        client.run();
    }
}
