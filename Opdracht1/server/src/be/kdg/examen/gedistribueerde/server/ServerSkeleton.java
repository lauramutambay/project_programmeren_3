package be.kdg.examen.gedistribueerde.server;

import be.kdg.examen.gedistribueerde.client.Document;
import be.kdg.examen.gedistribueerde.client.DocumentImpl;
import be.kdg.examen.gedistribueerde.communication.MessageManager;
import be.kdg.examen.gedistribueerde.communication.MethodCallMessage;
import java.awt.event.TextListener;

public class ServerSkeleton {
    private final MessageManager messageManager;
    private final Server server;

    /***
     * Er wordt een nieuwe Skeleton aangemaakt
     */
    public ServerSkeleton() {
        this.messageManager = new MessageManager();
        System.out.println("my address = " + messageManager.getMyAddress());
        this.server = new ServerImpl();
    }

    private void sendEmptyReply(MethodCallMessage request) {
        MethodCallMessage reply = new MethodCallMessage(messageManager.getMyAddress(), "result");
        reply.setParameter("result", "Ok");
        messageManager.send(reply, request.getOriginator());
    }

    /***
     * Behandeld de request die de "log" methode naam bevat
     *
     * @param request de request dat behandeld wordt
     */
    private void handleLog(MethodCallMessage request){
        String docText = request.getParameter("document");
        server.log(new DocumentImpl(docText));
        sendEmptyReply(request);
    }

    /***
     * Behandeld de request die de "create" methode naam bevat
     *
     * @param request de request dat behandeld wordt
     */
    private void handleCreate(MethodCallMessage request){
        String s = request.getParameter("s");
        String doc = server.create(s).getText();
        MethodCallMessage reply = new MethodCallMessage(messageManager.getMyAddress(), "result");
        reply.setParameter("result", doc);
        messageManager.send(reply, request.getOriginator());
    }

    /***
     * Behandeld de request die de "toUpper" methode naam bevat
     *
     * @param request de request dat behandeld wordt
     */
    private void handleToUpper(MethodCallMessage request){
        String docText = request.getParameter("document");
        Document doc = new DocumentImpl(docText);
        server.toUpper(doc);
        MethodCallMessage reply = new MethodCallMessage(messageManager.getMyAddress(), "result");
        reply.setParameter("result", doc.getText());
        messageManager.send(reply, request.getOriginator());
    }
    /***
     * Behandeld de request die de "toLower" methode naam bevat
     *
     * @param request de request dat behandeld wordt
     */
    private void handleToLower(MethodCallMessage request){
        String docText = request.getParameter("document");
        DocumentImpl doc = new DocumentImpl(docText);
        TextListener textListener = e -> {
            MethodCallMessage reply = new MethodCallMessage(messageManager.getMyAddress(), "result");
            reply.setParameter("result", doc.getText());
            messageManager.send(reply, request.getOriginator());
        };
        doc.setTextListener(textListener);
        server.toLower(doc);
        /*String docText = request.getParameter("document");
        Document doc = new DocumentImpl(docText);
        server.toLower(doc);
        MethodCallMessage reply = new MethodCallMessage(messageManager.getMyAddress(), "result");
        reply.setParameter("result", doc.getText());
        messageManager.send(reply, request.getOriginator());*/

    }
    /***
     * Behandeld de request die de "type" methode naam bevat
     *
     * @param request de request dat behandeld wordt
     */
    private void handleType(MethodCallMessage request){
        String docText = request.getParameter("document");
        String text = request.getParameter("text");
        DocumentImpl doc = new DocumentImpl(docText);
        TextListener textListener = e -> {
            MethodCallMessage reply = new MethodCallMessage(messageManager.getMyAddress(), "result");
            reply.setParameter("result", doc.getText());
            messageManager.send(reply, request.getOriginator());
        };
        doc.setTextListener(textListener);
        server.type(doc,text);

    }

    /**
     * Handles an incomming request.
     *
     * @param request the request that is being handled.
     */
    private void handleRequest(MethodCallMessage request) {
        //System.out.println("ServerSkeleton:handleRequest(" + request + ")");
        String methodName = request.getMethodName();
        if ("log".equals(methodName)) {
            handleLog(request);
        } else if ("create".equals(methodName)) {
            handleCreate(request);
        } else if ("toUpper".equals(methodName)) {
            handleToUpper(request);
        } else if ("toLower".equals(methodName)) {
            handleToLower(request);
        }else if ("type".equals(methodName)) {
            handleType(request);
        } else {
            System.out.println("ServerSkeleton: received an unknown request:");
            System.out.println(request);
        }
    }

    /***
     * Gaat steeds kijken of er binnekomende requests zijn
     *
     */

    private void run() {
        while (true) {
            MethodCallMessage request = messageManager.wReceive();
            handleRequest(request);
        }
    }

    public static void main(String[] args) {
        ServerSkeleton serverSkeleton = new ServerSkeleton();
        serverSkeleton.run();
    }
}
