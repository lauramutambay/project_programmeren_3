package be.kdg.distrib.stubFactory;

import be.kdg.distrib.communication.MessageManager;
import be.kdg.distrib.communication.MethodCallMessage;
import be.kdg.distrib.communication.NetworkAddress;

import java.lang.reflect.*;
import java.util.*;

public class StubInvocationHandler implements InvocationHandler {
    private NetworkAddress networkAddress;
    private MessageManager messageManager;
    private static final Map<Class<?>, Class<?>> WRAPPER_TYPE_MAP;

    public StubInvocationHandler(NetworkAddress networkAddress) {
        this.networkAddress = networkAddress;
        messageManager = new MessageManager();
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {

        MethodCallMessage message = new MethodCallMessage(messageManager.getMyAddress(), method.getName());
        if (args != null) {
            for (int i = 0; i < args.length; i++) {
                if (!WRAPPER_TYPE_MAP.containsKey(args[i].getClass())) {
                    for (Field field : args[i].getClass().getDeclaredFields()) {
                        field.setAccessible(true);
                        message.setParameter("arg" + i + "." + field.getName(), field.get(args[i]).toString());
                    }
                } else {
                    message.setParameter("arg" + i, args[i].toString());
                }

            }
        }
        messageManager.send(message, networkAddress);
        message = messageManager.wReceive();
        if (method.getReturnType().equals(void.class)) {
            return null;
        } else if (WRAPPER_TYPE_MAP.containsValue(method.getReturnType())) {
            return parseObjectToType(method.getReturnType(), message.getParameter("result"));
        } else {
            Object object = method.getReturnType().getDeclaredConstructor().newInstance();
            for (Object parameter : message.getParameters().keySet()) {
                Field resultField =method.getReturnType().getDeclaredField(parameter.toString().substring(7));
                resultField.setAccessible(true);
                resultField.set(object,parseObjectToType(resultField.getType(),message.getParameters().get(parameter.toString())));
            }
            return object;
        }
    }

    static {
        WRAPPER_TYPE_MAP = new HashMap<>(16);
        WRAPPER_TYPE_MAP.put(Integer.class, int.class);
        WRAPPER_TYPE_MAP.put(Byte.class, byte.class);
        WRAPPER_TYPE_MAP.put(Character.class, char.class);
        WRAPPER_TYPE_MAP.put(Boolean.class, boolean.class);
        WRAPPER_TYPE_MAP.put(Double.class, double.class);
        WRAPPER_TYPE_MAP.put(Float.class, float.class);
        WRAPPER_TYPE_MAP.put(Long.class, long.class);
        WRAPPER_TYPE_MAP.put(Short.class, short.class);
        WRAPPER_TYPE_MAP.put(Void.class, void.class);
        WRAPPER_TYPE_MAP.put(String.class, String.class);
    }

    public static Object parseObjectToType(Class<?> clazz, String value) {
        if (Boolean.class == clazz || boolean.class == clazz) return Boolean.parseBoolean(value);
        if (Byte.class == clazz || byte.class == clazz) return Byte.parseByte(value);
        if (Short.class == clazz || short.class == clazz) return Short.parseShort(value);
        if (Integer.class == clazz || int.class == clazz) return Integer.parseInt(value);
        if (Long.class == clazz || long.class == clazz) return Long.parseLong(value);
        if (Float.class == clazz || float.class == clazz) return Float.parseFloat(value);
        if (Double.class == clazz || double.class == clazz) return Double.parseDouble(value);
        if (Character.class == clazz || char.class == clazz) return value.charAt(0);
        return value;
    }

}
