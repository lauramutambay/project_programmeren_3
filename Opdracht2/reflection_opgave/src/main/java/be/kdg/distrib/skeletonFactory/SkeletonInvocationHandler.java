package be.kdg.distrib.skeletonFactory;

import be.kdg.distrib.communication.MessageManager;
import be.kdg.distrib.communication.MethodCallMessage;
import be.kdg.distrib.communication.NetworkAddress;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;

public class SkeletonInvocationHandler implements InvocationHandler {
    private Object incomingObject;
    private MessageManager messageManager;
    private static final Map<Class<?>, Class<?>> WRAPPER_TYPE_MAP;

    public SkeletonInvocationHandler(Object object) {
        this.incomingObject = object;
        messageManager = new MessageManager();
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        //getAddress()
        if (method.getReturnType().equals(NetworkAddress.class)) {
            return messageManager.getMyAddress();
        }


        //run()
        if (method.getName().equals("run")) {
            Thread thread = new Thread(() -> {
                while (true) {
                    MethodCallMessage mmc = messageManager.wReceive();
                    try {
                        handleRequest(mmc,incomingObject);
                    } catch (Throwable e) {
                        e.printStackTrace();
                    }
                }
            });
            thread.start();

        }
        //handleRequest
        if (args != null && args[0] instanceof MethodCallMessage) {
            handleRequest((MethodCallMessage) args[0], incomingObject);
        }

        return null;
    }



    private void handleRequest(MethodCallMessage methodCallMessage, Object incomingObject) throws Throwable {
        boolean isMethodFound = false;
        for (Method requestMethod : incomingObject.getClass().getMethods()) {
            if (requestMethod.getName().equals(methodCallMessage.getMethodName())) {
                isMethodFound = true;
                Object[] methodParameterCount = new Object[requestMethod.getParameterCount()];

                int methodParameterCountInArgs = (int) methodCallMessage.getParameters().keySet().stream().map(p -> p.substring(0, 4)).distinct().count();

                if (methodParameterCountInArgs != requestMethod.getParameterCount())
                    throw new IllegalArgumentException();

                for (int i = 0; i < requestMethod.getParameterCount(); i++) {
                    //check of het parametertype een object is
                    if (!WRAPPER_TYPE_MAP.containsValue(requestMethod.getParameterTypes()[i])) {
                        //nieuwe instantie maken van het object
                        Object parameterObject = requestMethod.getParameters()[i].getType().getDeclaredConstructor().newInstance();
                        //de fields tellen van het object
                        int parameterObjectFieldCount = parameterObject.getClass().getDeclaredFields().length;
                        Object[] fieldObjectArray = new Object[parameterObjectFieldCount];

                        for (int j = 0; j < parameterObjectFieldCount; j++) {
                            String fieldName = methodCallMessage.getParameters().keySet().toArray()[i + j].toString().substring(5);
                            String argumentName = methodCallMessage.getParameters().keySet().toArray()[j + i].toString();
                            //haal het juiste binnenkomende argument value op
                            fieldObjectArray[j] = methodCallMessage.getParameters().get(argumentName);

                            //zet het field om naar het juiste Type
                            fieldObjectArray[j] = parseObjectToType(requestMethod.getParameterTypes()[i].getDeclaredField(fieldName).getType(), fieldObjectArray[j].toString());

                            Field currentField = parameterObject.getClass().getDeclaredField(fieldName);
                            currentField.setAccessible(true);
                            currentField.set(parameterObject, fieldObjectArray[j]);
                            methodParameterCount[i] = parameterObject;
                        }

                    } else if (methodCallMessage.getParameters().keySet().stream()
                            .anyMatch(k -> k.contains("arg"))) {
                        final String parameterName = "arg" + i;
                        methodParameterCount[i] = methodCallMessage.getParameters().get(parameterName);
                        methodParameterCount[i] = parseObjectToType(requestMethod.getParameters()[i].getType(), methodParameterCount[i].toString());
                    } else {
                        throw new IllegalArgumentException();
                    }
                }
                Object resultObject = requestMethod.invoke(incomingObject, methodParameterCount);
                if (requestMethod.getReturnType().equals(void.class)) {
                    sendEmptyReply(methodCallMessage);
                } //als je een object moet returnen
                else if (!WRAPPER_TYPE_MAP.containsValue(requestMethod.getReturnType())) {
                    MethodCallMessage reply = new MethodCallMessage(messageManager.getMyAddress(), "result");
                    for (Field declaredField : requestMethod.getReturnType().getDeclaredFields()) {
                        declaredField.setAccessible(true);
                        reply.setParameter("result." + declaredField.getName(), declaredField.get(resultObject).toString());
                    }
                    messageManager.send(reply, methodCallMessage.getOriginator());
                } else {
                    MethodCallMessage reply = new MethodCallMessage(messageManager.getMyAddress(), "result");
                    reply.setParameter("result", resultObject.toString());
                    messageManager.send(reply, methodCallMessage.getOriginator());
                }
            }
        }
        if (!isMethodFound) throw new IllegalArgumentException();

    }
    private void sendEmptyReply(MethodCallMessage request) {
        MethodCallMessage reply = new MethodCallMessage(messageManager.getMyAddress(), "result");
        reply.setParameter("result", "Ok");
        messageManager.send(reply, request.getOriginator());
    }

    public static Object parseObjectToType(Class<?> clazz, String value) {
        if (Boolean.class == clazz || boolean.class == clazz) return Boolean.parseBoolean(value);
        if (Byte.class == clazz || byte.class == clazz) return Byte.parseByte(value);
        if (Short.class == clazz || short.class == clazz) return Short.parseShort(value);
        if (Integer.class == clazz || int.class == clazz) return Integer.parseInt(value);
        if (Long.class == clazz || long.class == clazz) return Long.parseLong(value);
        if (Float.class == clazz || float.class == clazz) return Float.parseFloat(value);
        if (Double.class == clazz || double.class == clazz) return Double.parseDouble(value);
        if (Character.class == clazz || char.class == clazz) return value.charAt(0);
        return value;
    }

    static {
        WRAPPER_TYPE_MAP = new HashMap<>(16);
        WRAPPER_TYPE_MAP.put(Integer.class, int.class);
        WRAPPER_TYPE_MAP.put(Byte.class, byte.class);
        WRAPPER_TYPE_MAP.put(Character.class, char.class);
        WRAPPER_TYPE_MAP.put(Boolean.class, boolean.class);
        WRAPPER_TYPE_MAP.put(Double.class, double.class);
        WRAPPER_TYPE_MAP.put(Float.class, float.class);
        WRAPPER_TYPE_MAP.put(Long.class, long.class);
        WRAPPER_TYPE_MAP.put(Short.class, short.class);
        WRAPPER_TYPE_MAP.put(Void.class, void.class);
        WRAPPER_TYPE_MAP.put(String.class, String.class);
    }
}
