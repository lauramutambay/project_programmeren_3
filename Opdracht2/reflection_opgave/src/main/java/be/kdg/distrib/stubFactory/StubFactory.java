package be.kdg.distrib.stubFactory;

import be.kdg.distrib.communication.MessageManager;
import be.kdg.distrib.communication.NetworkAddress;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Proxy;

public class StubFactory {
    public static Object createStub(Class<?> myClass, String s, int port) {
        NetworkAddress address = new NetworkAddress(s, port);
        InvocationHandler handler = new StubInvocationHandler(address);
        return Proxy.newProxyInstance(myClass.getClassLoader(), new Class[] {myClass}, handler);

    }
}
