package be.kdg.distrib.skeletonFactory;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;

public class SkeletonFactory {
    public static Object createSkeleton(Object obj) {
        InvocationHandler handler = new SkeletonInvocationHandler(obj);
        return Proxy.newProxyInstance(Thread.currentThread().getContextClassLoader(),new Class[]{Skeleton.class}, handler);
    }
}
